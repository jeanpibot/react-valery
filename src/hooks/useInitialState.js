import { useState, useEffect } from 'react';
import axios from 'axios';
import initialState from '../initialState';

const API = 'https://fakestoreapi.com/products';

const useInitialState = () => {
  const [state, setState] = useState(initialState);
  const [products, setProducts] = useState([]);

  useEffect(async () => {
    const response = await axios(API);
    setProducts(response.data);
  }, []);

  const addToBasket = (payload) => {
    setState({
      ...state,
      basket: [...state.basket, payload],
    });
  };

  const removeFromBasket = (payload) => {
    setState({
      ...state,
      basket: state.basket.filter((items) => items.id !== payload.id),
    });
  };

  const addToBuyer = (payload) => {
    setState({
      ...state,
      buyer: [...state.buyer, payload],
    });
  };

  const addNewOrder = (payload) => {
    setState({
      ...state,
      orders: [...state.order, payload],
    });
  };

  return {
    addToBasket,
    removeFromBasket,
    addToBuyer,
    addNewOrder,
    state,
    products,
  };
};

export default useInitialState;
