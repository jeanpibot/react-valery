const handleSumTotal = (basket) => {
  const reducer = (accumulator, currentValue) =>
    accumulator + currentValue.price;

  const sum = basket.reduce(reducer, 0);
  return sum;
};

export default handleSumTotal;
