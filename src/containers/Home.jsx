import React from 'react';
//components
import Products from '../components/Products';
//Initial state
import initialState from '../initialState';
// Helmet
import { Helmet } from 'react-helmet';

const Home = () => {
  return (
    <>
      <Helmet>
        <link rel="shortcut-icon" type="image/png" href="../favicon.ico" />
        <meta name="og:title" content="valery tienda" />
        <meta name="og:description" content="La mejor tienda online" />
        <meta name="og:url" content="" />
        <meta name="og:site_name" content="Valery Tienda" />
        <meta name="og:locate" content="es_ES" />
        <meta name="og:type" content="article" />
        <meta name="fb:app_id" content="" />
      </Helmet>
      <Products products={initialState.products} />
    </>
  )
};

export default Home;
