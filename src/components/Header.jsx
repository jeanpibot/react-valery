import React, { useContext } from 'react';
import { Link } from 'react-router-dom';
//Styles
import '../styles/header.css';
// AppContext
import AppContext from '../context/AppContext';

const Header = () => {
  const { state } = useContext(AppContext);
  const { basket } = state;

  return (
    <div className="header">
      <figure className="header_logo">
        <Link to="/">
          <img
            src="https://i.ibb.co/7kqvpT2/logo.png"
            alt="logo-Valery"
            border="0"
          />
        </Link>
      </figure>
      <div className="header_checkout">
        <Link to="/location">
          <i className="fas fa-map-marker-alt"></i>
        </Link>
      </div>
      <div className="header_checkout">
        <Link to="/checkout">
          <i className="fas fa-shopping-basket"></i>
        </Link>
        {basket.length > 0 && (
          <div className="header_alert">{basket.length}</div>
        )}
      </div>
      <div className="header_social">
        <ul>
          <li>
            <a
              href="https://www.instagram.com/valery_tienda1/"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-instagram"></i>
            </a>
          </li>
          <li>
            <a
              href="https://www.facebook.com/paola.escalantesalas"
              target="_blank"
              rel="noreferrer"
            >
              <i className="fab fa-facebook"></i>
            </a>
          </li>
        </ul>
      </div>
    </div>
  );
};

export default Header;
