import React from 'react';
//components
import Header from './Header';
import Footer from './Footer';
//Styles
import '../styles/layout.css';

const Layout = ({ children }) => {
  return (
    <>
      <Header />
      <div className="main">{children}</div>
      <Footer />
    </>
  );
};

export default Layout;
