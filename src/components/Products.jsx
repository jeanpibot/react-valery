import React, {
  useContext,
  useState,
  useRef,
  useMemo,
  useCallback,
} from 'react';
//components
import Product from './Product';
import Search from './Search';
//AppContext
import AppContext from '../context/AppContext';
//Styles
import '../styles/products.css';

const Products = () => {
  const { products, addToBasket } = useContext(AppContext);
  const [search, setSearch] = useState('');
  const searchInput = useRef(null);

  const handleAddToBasket = (product) => () => {
    addToBasket(product);
  };

  const handleSearch = useCallback(() => {
    setSearch(searchInput.current.value);
  }, []);

  const filteredProducts = useMemo(
    () =>
      products.filter((product) => {
        return product.title.toLowerCase().includes(search.toLowerCase());
      }),
    [products, search]
  );

  return (
    <>
      <Search
        search={search}
        searchInput={searchInput}
        handleSearch={handleSearch}
      />
      <div className="products">
        <div className="products-items">
          {filteredProducts.map((product) => (
            <Product
              key={product.id}
              product={product}
              handleAddToBasket={handleAddToBasket}
            />
          ))}
        </div>
      </div>
    </>
  );
};

export default Products;
