import React from 'react';
//styles
import '../styles/footer.css';

const Footer = () => {
  return (
    <div className="footer">
      <div className="footer_copyright">
        <span>
          © 2021 Valery Tienda | Todos los derechos reservados. Teléfono (57)
          3214537245. Dirección: Calle 13 #11-50, Florencia Caquetá Colombia.
        </span>
      </div>
    </div>
  );
};

export default Footer;
